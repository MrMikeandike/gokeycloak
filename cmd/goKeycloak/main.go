package main
import (
	"github.com/Nerzal/gocloak/v3"
	//jwt "github.com/dgrijalva/jwt-go"
	
	"log"
	
	"github.com/joho/godotenv"
	"os"
	
	
)
func main() {
	err := godotenv.Load("./cmd/goKeycloak/.env")
	if err != nil {
		log.Fatal(err)
	}
		
	
	issuer := os.Getenv("KEYCLOAK_ISSUER")
	clientID := os.Getenv("KEYCLOAK_CLIENT_ID")
	//audience := os.Getenv("KEYCLOAK_AUDIENCE")
	log.Println(issuer)

	client := gocloak.NewClient(os.Getenv("KEYCLOAK_HOSTNAME"))
	parsedToken, parsedClaims, err := client.DecodeAccessToken(os.Getenv("KEYCLOAK_TOKEN"), os.Getenv("KEYCLOAK_REALM"))
	if err != nil {
		log.Println("printing error for decodeAccessToken")
		log.Fatal(err)
	}
	log.Println("printing token")
	log.Println(parsedToken)
	log.Println("printing claims")
	log.Println(parsedClaims)
	if err := parsedClaims.Valid(); err != nil {
		log.Println("not valid!!!")
		log.Println(err)
	} else {
		log.Println("Valid!!!")
	}
	_, _ = issuer, clientID
	// claims := &jwt.StandardClaims{
	// 		Issuer: issuer,
	// 		Id: clientID,
	// 		Audience: audience,
	// }
//	jwt.ParseWithClaims(token, claims, jwt.Keyfunc


}

