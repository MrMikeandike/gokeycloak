module bitbucket.org/MrMikeandike/gokeycloak

go 1.13

require (
	github.com/Nerzal/gocloak/v3 v3.7.0
	github.com/coreos/go-oidc v2.1.0+incompatible
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/joho/godotenv v1.3.0
	github.com/pquerna/cachecontrol v0.0.0-20180517163645-1555304b9b35 // indirect
	golang.org/x/crypto v0.0.0-20191122220453-ac88ee75c92c // indirect
	golang.org/x/oauth2 v0.0.0-20191122200657-5d9234df094c
	gopkg.in/square/go-jose.v2 v2.4.0 // indirect
)
